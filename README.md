# R package: "alternativeROC"


## Description

The [R](https://r-project.org/) package `SQU4REstats` provides a series of generic statistical functions.

## Version

The current version is 0.8.

## Installation

### From bitbucket

`SQU4REstats` can be installed directly from bitbucket. If you've installed and loaded the `devtools` package, the installation command is :

`devtools::install_bitbucket("SQU4RE_stats/squ4restats",subdir="SQU4REstats")`

## Example

```R
require(SQU4REstats)
SQ4boxplot(split(rnorm(123),runif(123)<.5),col=2:3)
```

## Package contents

-
